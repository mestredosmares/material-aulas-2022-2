
window.onload = () =>  {

  const btn_criar = document.querySelector("#btn_criar");
  

  if (btn_criar) {
    btn_criar.addEventListener('click', ()=>{
      const codigo_jogador = document.querySelector('#numero')?.value || 0;
      document.cookie = "jogador=" + codigo_jogador;
    });
  }


  const mostra_cookie = document.querySelector('#mostra_cookie');

  if (mostra_cookie) {
     cookies = document.cookie
     mostra_cookie.innerHTML += cookies.split(";").find((e) => e.includes("jogador")).split("=")[1]
  }
}