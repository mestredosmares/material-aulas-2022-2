let meu_vetor = ['primeiro elemento', 2];

console.log('meu_vetor.length', meu_vetor.length);

let outro_vetor = new Array();
console.log('outro_vetor.length', outro_vetor.length);
outro_vetor[0] = 'primeiro elemento';
outro_vetor[2] = 3
console.log('outro_vetor', outro_vetor);
console.log('outro_vetor[1]', outro_vetor[1]);

const eh_novo = (testado) => (testado.toLowerCase() === "novo")? true : false;

const alerta_se_novo = () => {
    const meu_texto = document.querySelector('#meu_texto').value;

    if (eh_novo(meu_texto)) {
        alert('Quem dera');
    } else {
        muda_texto();
    }
}

function muda_texto(){
    const meu_texto = document.querySelector('#meu_texto').value;
    const meu_span = document.querySelector('#paragrafos > p:nth-child(1) > span:nth-child(1)');
    meu_span.innerHTML = meu_texto;
}

window.onload = function(){
    document.getElementById('colorida').style.backgroundColor = "red";

    //criando botão
    const meu_botao = document.createElement('input');
    meu_botao.type = 'button';
    meu_botao.value = 'Testar';
    meu_botao.addEventListener('click', alerta_se_novo)
    document.getElementById('inputs').appendChild(meu_botao);

    const lista = document.getElementById('minha_lista');

    let i = 0;
    let el;
    //for(let i = 0; i < 10; i++){
    while(i<10){
        el = document.createElement('li');
        el.innerHTML = 'coisa ' + i;
        lista.appendChild(el);
        i++;
    }
}

