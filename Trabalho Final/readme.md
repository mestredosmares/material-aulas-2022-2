# Trabalho Final 2022-2

##  Objetivos Gerais
1. Construir um website dinâmico a partir de dados formatados
1. O website precisa possuir duas "páginas": 
   - uma para exibir a listagem dos jogadores
   - uma para exibir os detalhes de cada jogador
1. Os alunos devem ser criativos e exibir os dados com formatação atraente
1. O conteúdo da página de listagem deve ser construído dinâmicamente
1. Os estilos devem estar em um arquivo CSS próprio
1. Os comando javascript devem estar em um arquivo próprio

## Página de listagem de jogadores
1. Informações exibidas:
   - nome
   - posicao
   - imagem
1. As informações de cada um dos jogadores deve estar organizada espacialmente de forma coerente em um Card
1. A quantidade de Cards por linha deve variar e ser ajustada de acordo com a largura disponível na janela do navegador
   - **ATENÇÃO:** o desenvolvimento deve ser _mobile first_
1. Deve ser possível **filtrar** os jogadores que serão listados
1. Ao clicar em um jogador, o usuário deve ser direcionado a página com informações do jogador específico
1. Os dados dos jogadores estão em [arquivo com dados](https://gitlab.com/i2311/desenvolvimento-web/material-aulas-2022-2/-/blob/main/Trabalho%20Final/jogadores.json)

## Página do jogador
1. Informações exibidas
   - imagem
   - nome_completo
   - nascimento
   - altura_peso
1. A página do jogador deve ser criada dinâmicamente com base no jogador escolhido na página da listagem
   - **DICA:** use _cookies_

## Entrega
1. link do repostório do gitlab
1. link da página (deve estar online no gitlab pages)
1. enviar por email para eduardo@mangeli.com.br com o assunto [Trabalho Final 2022-2 Desenvolvimento Web]
